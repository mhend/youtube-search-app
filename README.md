# YouTube Searcher

This app made with the [Modern React with Redux](https://www.udemy.com/react-redux/) class on Udemy by Stephen Grider. Very nice class on using React.

### Running this app

To run this app, simply clone the repo and run:

```
> npm install
> npm start
```

